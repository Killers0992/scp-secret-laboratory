using System;
using System.Globalization;
using uMod.Auth;
using uMod.Common;
using uMod.Unity;
using UnityEngine;

namespace uMod.Game.SCPSecretLaboratory
{
    /// <summary>
    /// Represents a player, either connected or not
    /// </summary>
    public class SCPSecretLaboratoryPlayer : UniversalPlayer, IPlayer
    {
        #region Initialization

        internal ReferenceHub referenceHub;

        public SCPSecretLaboratoryPlayer(string userId, string playerName)
        {
            // Store player details
            Id = userId;
            Name = playerName.Sanitize();
        }

        public SCPSecretLaboratoryPlayer(ReferenceHub referenceHub) : this(referenceHub.characterClassManager.UserId, referenceHub.nicknameSync.MyNick)
        {
            // Store player object
            this.referenceHub = referenceHub;
        }

        #endregion Initialization

        #region Objects

        /// <summary>
        /// Gets/sets the object that backs the player
        /// </summary>
        public object Object { get; set; }

        /// <summary>
        /// Gets the player's last command type
        /// </summary>
        public CommandType LastCommand { get; set; }

        /// <summary>
        /// Reconnects the gamePlayer to the player object
        /// </summary>
        /// <param name="gamePlayer"></param>
        public void Reconnect(object gamePlayer)
        {
            // Reconnect player objects
            Object = gamePlayer;
            referenceHub = gamePlayer as ReferenceHub;
        }

        #endregion Objects

        #region Information

        /// <summary>
        /// Gets/sets the name for the player
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the ID for the player (unique within the current game)
        /// </summary>
        public override string Id { get; }

        /// <summary>
        /// Gets the language for the player
        /// </summary>
        public CultureInfo Language => CultureInfo.GetCultureInfo("en");

        /// <summary>
        /// Gets the IP address for the player
        /// </summary>
        public string Address => referenceHub.characterClassManager.connectionToClient.address;

        /// <summary>
        /// Gets the average network ping for the player
        /// </summary>
        public int Ping => 0;

        /// <summary>
        /// Gets if the player is a server admin
        /// </summary>
        public override bool IsAdmin => referenceHub.serverRoles.BypassStaff;

        /// <summary>
        /// Gets if the player is a server moderator
        /// </summary>
        public override bool IsModerator => referenceHub.serverRoles.RemoteAdmin;

        /// <summary>
        /// Gets if the player is banned
        /// </summary>
        public bool IsBanned => BanHandler.GetBan(Id, BanHandler.BanType.UserId) != null || BanHandler.GetBan(Address, BanHandler.BanType.IP) != null;

        /// <summary>
        /// Gets if the player is connected
        /// </summary>
        public bool IsConnected => referenceHub != null; // TODO: Check for a better way

        /// <summary>
        /// Gets if the player is alive
        /// </summary>
        public bool IsAlive => referenceHub != null && referenceHub.characterClassManager.IsAlive;

        /// <summary>
        /// Gets if the player is dead
        /// </summary>
        public bool IsDead => referenceHub != null && !referenceHub.characterClassManager.IsAlive;

        /// <summary>
        /// Gets if the player is sleeping
        /// </summary>
        public bool IsSleeping => false; // TODO: Implement when possible

        /// <summary>
        /// Gets if the player is the server
        /// </summary>
        public bool IsServer => referenceHub != null && !referenceHub.isDedicatedServer;

        /// <summary>
        /// Gets/sets if the player has connected before
        /// </summary>
        public bool IsReturningPlayer { get; set; }

        /// <summary>
        /// Gets/sets if the player has spawned before
        /// </summary>
        public bool HasSpawnedBefore { get; set; }

        #endregion Information

        #region Administration

        /// <summary>
        /// Bans the player for the specified reason and duration
        /// </summary>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        public void Ban(string reason = "", TimeSpan duration = default)
        {
            // Check if already banned
            if (!IsBanned)
            {
                BanHandler.IssueBan(new BanDetails()
                {
                    Id = Id,
                    Expires = TimeBehaviour.GetBanExpirationTime((uint)duration.Seconds),
                    IssuanceTime = TimeBehaviour.CurrentTimestamp(),
                    Issuer = "Dedicated Server",
                    OriginalName = Name,
                    Reason = reason
                }, BanHandler.BanType.UserId);

                // Kick player with reason
                Kick(reason);
            }
        }

        /// <summary>
        /// Gets the amount of time remaining on the player's ban
        /// </summary>
        public TimeSpan BanTimeRemaining => IsBanned ? new TimeSpan(BanHandler.GetBan(Id, BanHandler.BanType.UserId).Expires - TimeBehaviour.CurrentTimestamp()) : TimeSpan.Zero;

        /// <summary>
        /// Kicks the player from the server
        /// </summary>
        /// <param name="reason"></param>
        public void Kick(string reason = "")
        {
            if (IsConnected)
            {
                ServerConsole.Disconnect(referenceHub.gameObject, reason);
            }
        }

        /// <summary>
        /// Unbans the player
        /// </summary>
        public void Unban()
        {
            // Check if already unbanned
            if (IsBanned)
            {
                // Unban player
                BanHandler.RemoveBan(Id, BanHandler.BanType.UserId);
            }
        }

        #endregion Administration

        #region Character

        /// <summary>
        /// Gets/sets the player's health
        /// </summary>
        public float Health
        {
            get => referenceHub != null ? referenceHub.playerStats.Health : 0f;
            set
            {
                if (referenceHub != null)
                {
                    referenceHub.playerStats.Health = value > 0f ? value : 0f;
                }
            }
        }

        /// <summary>
        /// Gets/sets the player's maximum health
        /// </summary>
        public float MaxHealth
        {
            get => referenceHub != null ? referenceHub.playerStats.maxHP : 0f;
            set
            {
                if (referenceHub != null)
                {
                    referenceHub.playerStats.maxHP = (int)value;
                }
            }
        }

        /// <summary>
        /// Heals the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Heal(float amount)
        {
            if (referenceHub != null)
            {
                referenceHub.playerStats.HealHPAmount(amount);
            }
        }

        /// <summary>
        /// Damages the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Hurt(float amount)
        {
            referenceHub?.playerStats.HurtPlayer(new PlayerStats.HitInfo(amount, "Server", DamageTypes.None, 0), ReferenceHub.HostHub.gameObject);
        }

        /// <summary>
        /// Causes the player's character to die
        /// </summary>
        public void Kill()
        {
            referenceHub?.playerStats.HurtPlayer(new PlayerStats.HitInfo(9999999f, "Server", DamageTypes.None, 0), ReferenceHub.HostHub.gameObject);
        }

        /// <summary>
        /// Renames the player to specified name
        /// <param name="newName"></param>
        /// </summary>
        public void Rename(string newName)
        {
            if (referenceHub?.nicknameSync != null)
            {
                referenceHub.nicknameSync.DisplayName = newName;
                Name = newName;
            }
        }

        /// <summary>
        /// Resets the player's character stats
        /// </summary>
        public void Reset()
        {
            if (referenceHub != null)
            {
                referenceHub.playerStats.Health = referenceHub.playerStats.maxHP;
                referenceHub.inventory.Clear();
            }
        }

        /// <summary>
        /// Respawns the player's character
        /// </summary>
        public void Respawn()
        {
            if (referenceHub != null)
            {
                referenceHub.characterClassManager.NetworkCurClass = RoleType.ClassD;
            }
        }

        /// <summary>
        /// Respawns the player's character at specified position
        /// </summary>
        public void Respawn(Position pos)
        {
            if (referenceHub != null)
            {
                referenceHub.characterClassManager.NetworkCurClass = RoleType.ClassD;
                referenceHub.characterClassManager.NetworkDeathPosition = pos.ToVector3();
            }
        }

        #endregion Character

        #region Positional

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <returns></returns>
        public Position Position() => referenceHub?.transform?.position.ToPosition();

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Position(out float x, out float y, out float z)
        {
            Position pos = Position();
            x = pos.X;
            y = pos.Y;
            z = pos.Z;
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Teleport(float x, float y, float z)
        {
            if (IsConnected && referenceHub.characterClassManager.IsAlive && (referenceHub.characterClassManager.NetworkCurClass != RoleType.Spectator))
            {
                referenceHub.playerMovementSync.ForcePosition(new Vector3(x, y, z));
            }
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="pos"></param>
        public void Teleport(Position pos) => Teleport(pos.X, pos.Y, pos.Z);

        #endregion Positional

        #region Chat and Commands

        /// <summary>
        /// Sends the specified message and prefix to the player
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Message(string message, string prefix, params object[] args)
        {
            if (!string.IsNullOrEmpty(message) && IsConnected)
            {
                referenceHub.characterClassManager.TargetConsolePrint(referenceHub.characterClassManager.connectionToClient, message, "green");
            }
        }

        /// <summary>
        /// Sends the specified message to the player
        /// </summary>
        /// <param name="message"></param>
        public void Message(string message) => Message(message, null);

        /// <summary>
        /// Replies to the player with the specified message and prefix
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Reply(string message, string prefix, params object[] args) => Message(message, prefix, args);

        /// <summary>
        /// Replies to the player with the specified message
        /// </summary>
        /// <param name="message"></param>
        public void Reply(string message) => Reply(message, null);

        /// <summary>
        /// Runs the specified console command on the player
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        public void Command(string command, params object[] args)
        {
            if (!string.IsNullOrEmpty(command) && IsConnected)
            {
                //CommandProcessor.ProcessQuery($"{command} {string.Join(" ", args)}", player.queryProcessor._sender);
            }
        }

        #endregion Chat and Commands
    }
}

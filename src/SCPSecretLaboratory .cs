using uMod.Plugins;
using uMod.Plugins.Decorators;

namespace uMod.Game.SCPSecretLaboratory
{
    /// <summary>
    /// The core SCPSecretLaboratory plugin
    /// </summary>
    [HookDecorator(typeof(ServerDecorator))]
    public class SCPSecretLaboratory : Plugin
    {
        #region Initialization

        internal static readonly SCPSecretLaboratoryProvider Universal = SCPSecretLaboratoryProvider.Instance;

        private bool serverInitialized;

        /// <summary>
        /// Initializes a new instance of the SCPSecretLaboratory class
        /// </summary>
        public SCPSecretLaboratory()
        {
            // Set plugin info attributes
            Title = "SCP: Secret Laboratory";
            Author = SCPSecretLaboratoryExtension.AssemblyAuthors;
            Version = SCPSecretLaboratoryExtension.AssemblyVersion;
        }

        /// <summary>
        /// Called when the server is first initialized
        /// </summary>
        [Hook("OnServerInitialized")]
        private void OnServerInitialized()
        {
            // Add universal commands
            Universal.CommandSystem.DefaultCommands.Initialize(this);

            // Clean up invalid permission data
            permission.RegisterValidate(ExtensionMethods.IsSteamId);
            permission.CleanUp();

            serverInitialized = true;
        }

        #endregion Initialization
    }
}
